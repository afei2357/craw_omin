# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import os

class OmimCrawPipeline:
    def process_item(self, item, spider):
        return item
class OmimPipeline(object):
    def process_item(self, item, spider):
        path = os.path.join("result","omim.xls")
        if not os.path.exists("result"):
            os.mkdir("result")
        with open(path,'a') as fh:
            # fh.write(item["Location"]+"\t"+item["Phenotype"]+"\t"+item["Phenotype_MIM_number"]+"\t"+item["Inheritance"]+"\t"+item["Phenotype_mapping_key"]
            #          +"\t"+item["Gene_Locus"]+"\t"+item["Gene_Locus_MIM_number"])
            print ('================================================')
            print ('writing in pipeline')
            print (path)
            print ('================================================')
            fh.write(item['ctx'])
        return item
