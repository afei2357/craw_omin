# -*- coding: utf-8 -*-
import scrapy
import re,json,os
import collections
from ..items import OmimItem


class Scrap1Spider(scrapy.Spider):
    name='scrap1'
    custom_settings = {
        'ITEM_PIPELINES': {
            'omim_craw.pipelines.OmimPipeline': 300
        }
    }

    allowed_domains = ['omim.org']
    start_urls = ['http://omim.org/']


    def parse(self, response):
        url_start = 'https://omim.org/clinicalSynopsis/'
        i = 0
        with open('/var/www/test/craw_omim/phenotype.txt') as fh:
            for line in fh:
                line = line.strip()
                if line :
                    i += 1
                    if i > 2:
                        break
                    phe_num =  line
                    url = url_start + str(phe_num) 
                    print( url)
                    yield scrapy.Request(url, callback=self.parse_gene_page)
        # yield scrapy.Request("https://omim.org/search/?index=entry&search=prefix%3A%23&sort=number+asc&start=30&limit=100", callback=self.parse_gene_page)


    # 一共设置三层header，
    #header0是总的0级标题，比如 SAETHRE-CHOTZEN SYNDROME; SCS
    # header1 是1级标题，比如 INHERITANCE，GROWTH这样的标题
    # header2 是最末一层标题，是斜字母的文字，比如Height，Head，Face这样的
    # 最后是文字具体信息，它们以短横线开头的文字信息。

    def parse_gene_page(self, response):
        dct = collections.OrderedDict()
        clinicalSynopsis_num = response.url.split('/')[-1]

        if not os.path.exists('result/html'):
            os.makedirs('result/html')
        if not os.path.exists('result/json'):
            os.makedirs('result/json')

        with open('result/html/'+clinicalSynopsis_num+'.html','w') as out :
            out.write(response.text)
        dct[clinicalSynopsis_num] = collections.OrderedDict()
        header0 = response.xpath('//*[@id="content"]/div[1]/div[2]/div[3]/div[1]/div[3]/h3/span/text()')[0].extract().strip()
        dct['header'] = header0
        clinicalSynopsis_div = response.xpath('//*[@id="clinicalSynopsis"]')
        for div1 in clinicalSynopsis_div.xpath('div'):
            # INHERITANCE
            #//*[@id="clinicalSynopsis"]/div/div[1]/span/strong/text()
           div_header1 = div1.xpath('div[1]/span/strong/text()').extract()
           if div_header1:
                header1 = div1.xpath('div[1]/span/strong/text()').extract()[0].strip()
                text_div = div1.xpath('div[2]/div/span/text()')
                if text_div:
                    #text = div1.xpath('div[2]/div/span/text()').extract()[0].strip()
                    text = text_div.extract()[0].strip()
                    dct[clinicalSynopsis_num][header1] =  [text]
                else:
                    dct[clinicalSynopsis_num][header1] = []
                for div2 in div1.xpath('div[2]'):
                    # //*[@id="clinicalSynopsis"]/div/div[2]/div[1]/div[1]/span/em/text()
                    #header2 = div2.xpath('div[1]/span/em/text()').extract()[0].strip()
                    for div3 in div2.xpath('div'):
                        header2_div = div3.xpath('div[1]/span/em/text()')
                        if header2_div :
                            header2 = div3.xpath('div[1]/span/em/text()').extract()[0].strip()
                            text_lst = div3.xpath('div[2]/span/text()')
                            text_sort = list(set( [i.strip().replace('\n','') for i in  text_lst.extract()] ) )
                            text_sort.remove('') if '' in text_sort else text_sort
                            #dct[clinicalSynopsis_num][header1][header2] = text_sort
                            header2_dct = collections.OrderedDict()
                            header2_dct[header2] = text_sort
                            dct[clinicalSynopsis_num][header1].append(header2_dct)
                        text_div3_lst = div3.xpath('./span/text()')
                        if text_div3_lst :
                            text_div3 = list(set( [i.strip().replace('\n','') for i in  text_div3_lst.extract()] ) )
                            text_div3.remove('') if '' in text_div3 else text_div3
                            dct[clinicalSynopsis_num][header1].append(text_div3)

        with open('result/json/'+clinicalSynopsis_num+'.json','w') as out2 :
            json.dump(dct,out2)



    def parse_gene_page_old(self, response):

        url = response.url
        page_num = re.findall('start=(\d+)',url)[0]
        print( "=============crawing page %s================"%page_num)
        tr_ctx = ""
        all_trs = response.xpath('//*[@id="content"]/div[1]/div[*]/div[2]/div/table[@class="table table-bordered table-condensed small mim-table-padding"]/tbody/tr')
        Location =""
        Phenotype =""
        Phenotype_MIM_number = ""
        Inheritance = ""
        Phenotype_mapping_key = ""
        Gene_Locus = ""
        Gene_Locus_MIM_number= ""
        for tr in all_trs:
            try:
                Location = tr.xpath('td[1]/span/a/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Location wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                Location = 'None'
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )


            try:
                Phenotype = tr.xpath('td[2]/span/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Phenotype wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                Phenotype = 'None'
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )

            try:
                Phenotype_MIM_number = tr.xpath('td[3]/span/a/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Phenotype_MIM_number wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                Phenotype_MIM_number = 'None'
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )


            try:
                Inheritance = tr.xpath('td[4]/span/abbr/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Inheritance wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )
                Inheritance = 'None'

            try:
                Phenotype_mapping_key = tr.xpath('td[5]/span/abbr/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Phenotype_mapping_key wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )
                Phenotype_mapping_key = "None"

            try:
                Gene_Locus = tr.xpath('td[6]/span/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Gene_Locus wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )
                Gene_Locus = "None"

            try:
                Gene_Locus_MIM_number = tr.xpath('td[7]/span/a/text()').extract()[0].strip()
            except:
                print("++++++++++++++++++++++Gene_Locus_MIM_number wrong 1111 +++++++++++++++++++++++++++++++++++++++++++")
                print(Location + "\t" +Phenotype + "\t" +Phenotype_MIM_number )
                Gene_Locus_MIM_number = "None"

            tr_ctx += Location + "\t" + Phenotype + "\t" + Phenotype_MIM_number + "\t" + Inheritance + "\t" + \
                      "\t" + Phenotype_mapping_key + "\t" + Gene_Locus + "\t" + Gene_Locus_MIM_number + '\t' + page_num+"\n"

        item = OmimItem()
        item['ctx'] = tr_ctx
        print ("crawing in spider ------------------------------------------------------------------")

        yield item
