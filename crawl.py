import requests,time,os,json
import collections
from lxml import etree

# 抓取omim网页
def request_page(phenotype_num,ip,proxy='get_web'):
    headers = {
        #"User-Agent":"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 BIDUBrowser/8.7 Safari/537.36"
        "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 QIHU 360SE"
    }
    url = 'https://omim.org/clinicalSynopsis/'+phenotype_num
    try:
        response = requests.get(url,headers=headers,proxies={'http':ip,'https':ip},timeout=8)
        print('try the ip  ,',ip,'url:|'+url+'|')
        if response.status_code==200 :
            print('success ip  :',ip,' phenotype_num:',phenotype_num)
            return response
        if response.status_code==404 and proxy == 'get_web' :
            return response
    except Exception as e:
        print('fail to get web ,',ip,url)
        print(e)
    return None



#从淘宝购买的链接里获取代理ip
def get_proxy():
    while True:
        proxy_resp = requests.get('http://proxy.httpdaili.com/apinew.asp?ddbh=2514165445489782436')
        if proxy_resp.status_code == 200:
            proxy = proxy_resp.text 
            http_ips_lst = proxy.split()
            for ip in http_ips_lst: 
                ret = request_page('100820',ip, proxy='proxy')
                if ret :
                    return ip
        print('waiting for next ip : ')
        time.sleep(10)

start_exist__lst = []
if  os.path.exists('result/html'):
    sys_result = os.popen('ls result/html/*html').read()
    lst = sys_result.split()
    start_exist__lst = set( [i.split('/')[-1].split('.')[0] for i in lst ] )



# 一共设置三层header，
#header0是总的0级标题，比如 SAETHRE-CHOTZEN SYNDROME; SCS
# header1 是1级标题，比如 INHERITANCE，GROWTH这样的标题
# header2 是最末一层标题，是斜字母的文字，比如Height，Head，Face这样的
# 最后是文字具体信息，它们以短横线开头的文字信息。
def crawl_parse( ):
    url_start = 'https://omim.org/clinicalSynopsis/'
    i = 0
    time.sleep(0.3)
    with open('phenotype.txt') as fh:
        for line in fh:
            line = line.strip()
            if line :
                i += 1
                #if i > 20:
                    #break
                    
                phe_num =  line.split()[0]
                if phe_num in start_exist__lst:
                    continue 
                ip = get_proxy()
                print('get an ip  ', ip )
                # flag 为true的意思是爬取失败，需要重新爬
                flag = True
                j = 0
                while flag:
                    print('try time  :',j,"|"+phe_num+'|')
                    response = request_page(phe_num,ip,proxy='get_web')
                    if response and response.status_code==200 :
                        print('save  a page :',response.url)
                        save_html(response)
                        flag = False
                        continue 
                    if response and response.status_code==404 :
                        print('get a page 404 :',response.url)
                        with open('fail404.txt','a') as fail:
                            fail.write(phe_num + '\n')
                        flag = False
                        continue 

                    ip = get_proxy()
                    j +=1
                    flag = True

                    
                    if j > 5:  #同一个网页爬失败5次，就换下一个phe_num
                        flag = False 
                        j = 0
                        with open('fail.txt','a') as fail:
                            fail.write(phe_num + '\n')


def save_html(response):
    dct = collections.OrderedDict()
    clinicalSynopsis_num = response.url.split('/')[-1]
    print('saving url :'+ response.url)
    if not os.path.exists('result/html'):
        os.makedirs('result/html')
    if not os.path.exists('result/json'):
        os.makedirs('result/json')

    with open('result/html/'+clinicalSynopsis_num+'.html','w') as out :
        out.write(response.text)
    dct[clinicalSynopsis_num] = collections.OrderedDict()
    html = response.text
    tree = etree.HTML(html)
    header0 = tree.xpath('//*[@id="content"]/div[1]/div[2]/div[3]/div[1]/div[3]/h3/span/text()')[0].strip()
    dct['header'] = header0
    clinicalSynopsis_div = tree.xpath('//*[@id="clinicalSynopsis"]')
    for div1 in clinicalSynopsis_div[0].xpath('div'):
        # INHERITANCE
        #//*[@id="clinicalSynopsis"]/div/div[1]/span/strong/text()
       div_header1 = div1.xpath('div[1]/span/strong/text()')
       if div_header1:
            header1 = div1.xpath('div[1]/span/strong/text()')[0].strip()
            text_div = div1.xpath('div[2]/div/span/text()')
            if text_div:
                #text = div1.xpath('div[2]/div/span/text()').extract()[0].strip()
                text = text_div[0].strip()
                dct[clinicalSynopsis_num][header1] =  [text]
            else:
                dct[clinicalSynopsis_num][header1] = []
            for div2 in div1.xpath('div[2]'):
                # //*[@id="clinicalSynopsis"]/div/div[2]/div[1]/div[1]/span/em/text()
                #header2 = div2.xpath('div[1]/span/em/text()').extract()[0].strip()
                for div3 in div2.xpath('div'):
                    header2_div = div3.xpath('div[1]/span/em/text()')
                    if header2_div :
                        header2 = div3.xpath('div[1]/span/em/text()')[0].strip()
                        text_lst = div3.xpath('div[2]/span/text()')
                        text_sort = list(set( [i.strip().replace('\n','') for i in  text_lst] ) )
                        text_sort.remove('') if '' in text_sort else text_sort
                        #dct[clinicalSynopsis_num][header1][header2] = text_sort
                        header2_dct = collections.OrderedDict()
                        header2_dct[header2] = text_sort
                        dct[clinicalSynopsis_num][header1].append(header2_dct)
                    text_div3_lst = div3.xpath('./span/text()')
                    if text_div3_lst :
                        text_div3 = list(set( [i.strip().replace('\n','') for i in  text_div3_lst] ) )
                        text_div3.remove('') if '' in text_div3 else text_div3
                        dct[clinicalSynopsis_num][header1].append(text_div3)

    with open('result/json/'+clinicalSynopsis_num+'.json','w') as out2 :
        json.dump(dct,out2)

crawl_parse( )
